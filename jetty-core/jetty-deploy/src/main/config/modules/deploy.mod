[description]
Enables web application deployment from the $JETTY_BASE/webapps/ directory.

[depend]
server

[lib]
lib/jetty-deploy-${jetty.version}.jar

[xml]
etc/jetty-deploy.xml

