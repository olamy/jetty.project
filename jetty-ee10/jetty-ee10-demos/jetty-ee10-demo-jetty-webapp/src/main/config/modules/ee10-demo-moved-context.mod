# DO NOT EDIT - See: https://www.eclipse.org/jetty/documentation/current/startup-modules.html

[description]
Demonstrate a Moved Context Handler.

[tags]
demo

[depends]
ee10-deploy

[files]
basehome:modules/demo.d/ee10-demo-moved-context.xml|webapps/ee10-demo-moved-context.xml

