# DO NOT EDIT - See: https://www.eclipse.org/jetty/documentation/current/startup-modules.html

[description]
Enables the glassfish version of JSTL for all webapps.

[environment]
ee10

[lib]
lib/ee10-glassfish-jstl/*.jar
