# DO NOT EDIT - See: https://www.eclipse.org/jetty/documentation/current/startup-modules.html

[description]
Provides a DefaultAuthConfigFactory for jaspi

[environment]
ee10

[tags]
security

[depend]
ee10-security

[provide]
auth-config-factory

[xml]
etc/jaspi/jetty-ee10-jaspi-default.xml
