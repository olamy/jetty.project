# DO NOT EDIT - See: https://www.eclipse.org/jetty/documentation/current/startup-modules.html

[description]
Adds the Jetty EE10 JNDI reference factories

[environment]
ee10

[depend]
jndi

[lib]
lib/jetty-ee10-jndi-${jetty.version}.jar
