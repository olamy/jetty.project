[description]
Enables Servlet resource injection. 

[environment]
ee10

[depend]
server
jndi
ee10-security
ee10-webapp

[lib]
lib/jetty-ee10-plus-${jetty.version}.jar
lib/jakarta.transaction-api-2.0.1.jar
