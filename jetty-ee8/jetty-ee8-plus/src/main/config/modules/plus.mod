[description]
Enables Servlet 3.1 resource injection.

[environment]
ee8

[depend]
server
security
jndi
webapp

[lib]
lib/jetty-ee8-plus-${jetty.version}.jar
lib/jakarta.transaction-api-*.jar
