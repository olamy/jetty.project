# DO NOT EDIT - See: https://www.eclipse.org/jetty/documentation/current/startup-modules.html

[description]
Adds servlet standard security handling to the classpath.

[environment]
ee8

[depend]
server

[lib]
lib/jetty-ee8-security-${jetty.version}.jar
