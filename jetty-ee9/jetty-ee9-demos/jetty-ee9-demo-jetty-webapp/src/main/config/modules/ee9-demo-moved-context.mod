# DO NOT EDIT - See: https://www.eclipse.org/jetty/documentation/current/startup-modules.html

[description]
Demonstrate a Moved Context Handler.

[environment]
ee9

[tags]
demo

[depends]
ee9-deploy

[files]
basehome:modules/demo.d/ee9-demo-moved-context.xml|webapps/ee9-demo-moved-context.xml

