# DO NOT EDIT - See: https://www.eclipse.org/jetty/documentation/current/startup-modules.html

[description]
Demonstrate the rewrite module.

[environment]
ee9

[tags]
demo

[depends]
rewrite

[xml]
etc/ee9-demo-rewrite-rules.xml

[files]
basehome:modules/demo.d/ee9-demo-rewrite-rules.xml|etc/ee9-demo-rewrite-rules.xml

