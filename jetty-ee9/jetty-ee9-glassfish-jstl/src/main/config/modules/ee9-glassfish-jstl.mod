# DO NOT EDIT - See: https://www.eclipse.org/jetty/documentation/current/startup-modules.html

[description]
Enables the glassfish version of JSTL for all webapps.

[environment]
ee9

[lib]
lib/ee9-glassfish-jstl/*.jar
