# DO NOT EDIT - See: https://www.eclipse.org/jetty/documentation/current/startup-modules.html

[description]
Adds the Jetty EE9 JNDI reference factories

[environment]
ee9

[depend]
jndi

[lib]
lib/jetty-ee9-jndi-${jetty.version}.jar
