[description]
Enables Servlet 3.1 resource injection.

[environment]
ee9

[environment]
ee9

[depend]
server
jndi
ee9-security
ee9-webapp

[lib]
lib/jetty-ee9-plus-${jetty.version}.jar
lib/jakarta.transaction-api-2.0.0.jar
