# DO NOT EDIT - See: https://www.eclipse.org/jetty/documentation/current/startup-modules.html

[description]
Enables JSP for all web applications deployed on the server.

[environment]
ee10

[depend]
ee10-servlet
ee10-annotations
ee10-apache-jsp
