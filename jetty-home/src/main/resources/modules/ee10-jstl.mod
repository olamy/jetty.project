# DO NOT EDIT - See: https://www.eclipse.org/jetty/documentation/current/startup-modules.html

[description]
Enables JSTL for all web applications deployed on the server.

[environment]
ee10

[depend]
ee10-jsp
ee10-glassfish-jstl
